https://www.djangoproject.com/

## Stats
https://pypistats.org/packages/django


## Install
$ python -m pip install Django

## Create
$ django-admin startproject myproject

## Run
python manage.py runserver
